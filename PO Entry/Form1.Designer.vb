﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem3 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Me.RadSplitContainer1 = New Telerik.WinControls.UI.RadSplitContainer()
        Me.SplitPanel1 = New Telerik.WinControls.UI.SplitPanel()
        Me.grp_orderForm = New Telerik.WinControls.UI.RadGroupBox()
        Me.btn_submitOrder = New Telerik.WinControls.UI.RadButton()
        Me.btn_confirmDetails = New System.Windows.Forms.Button()
        Me.btn_GoBack = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_notes = New Telerik.WinControls.UI.RadTextBox()
        Me.lbl_vat = New System.Windows.Forms.Label()
        Me.drp_currency = New Telerik.WinControls.UI.RadDropDownList()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_total = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_convertTotal = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_payTerms = New System.Windows.Forms.TextBox()
        Me.drp_date = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_import = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_exchangeRate = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_commission = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_shippingCost = New System.Windows.Forms.TextBox()
        Me.drp_deliveryDate = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_conPackReq = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_contractPacking = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_email = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.drp_buyers = New Telerik.WinControls.UI.RadDropDownList()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_purchaseNumber = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_supplier = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_sageCode = New System.Windows.Forms.TextBox()
        Me.SplitPanel2 = New Telerik.WinControls.UI.SplitPanel()
        Me.flp_ordersLines = New System.Windows.Forms.FlowLayoutPanel()
        Me.btn_viewPO = New Telerik.WinControls.UI.RadButton()
        Me.btn_createPO = New Telerik.WinControls.UI.RadButton()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadSplitContainer1.SuspendLayout()
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitPanel1.SuspendLayout()
        CType(Me.grp_orderForm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_orderForm.SuspendLayout()
        CType(Me.btn_submitOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_notes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.drp_currency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.drp_date, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.drp_deliveryDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.drp_buyers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitPanel2.SuspendLayout()
        CType(Me.btn_viewPO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_createPO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadSplitContainer1
        '
        Me.RadSplitContainer1.Controls.Add(Me.SplitPanel1)
        Me.RadSplitContainer1.Controls.Add(Me.SplitPanel2)
        Me.RadSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadSplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.RadSplitContainer1.Name = "RadSplitContainer1"
        Me.RadSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '
        '
        Me.RadSplitContainer1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadSplitContainer1.Size = New System.Drawing.Size(1838, 866)
        Me.RadSplitContainer1.SplitterWidth = 4
        Me.RadSplitContainer1.TabIndex = 0
        Me.RadSplitContainer1.TabStop = False
        Me.RadSplitContainer1.Text = "RadSplitContainer1"
        '
        'SplitPanel1
        '
        Me.SplitPanel1.Controls.Add(Me.grp_orderForm)
        Me.SplitPanel1.Location = New System.Drawing.Point(0, 0)
        Me.SplitPanel1.Name = "SplitPanel1"
        '
        '
        '
        Me.SplitPanel1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel1.Size = New System.Drawing.Size(1838, 208)
        Me.SplitPanel1.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, -0.2587007!)
        Me.SplitPanel1.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, -247)
        Me.SplitPanel1.TabIndex = 0
        Me.SplitPanel1.TabStop = False
        Me.SplitPanel1.Text = "SplitPanel1"
        '
        'grp_orderForm
        '
        Me.grp_orderForm.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.grp_orderForm.Controls.Add(Me.btn_submitOrder)
        Me.grp_orderForm.Controls.Add(Me.btn_confirmDetails)
        Me.grp_orderForm.Controls.Add(Me.btn_GoBack)
        Me.grp_orderForm.Controls.Add(Me.Label19)
        Me.grp_orderForm.Controls.Add(Me.txt_notes)
        Me.grp_orderForm.Controls.Add(Me.lbl_vat)
        Me.grp_orderForm.Controls.Add(Me.drp_currency)
        Me.grp_orderForm.Controls.Add(Me.Label18)
        Me.grp_orderForm.Controls.Add(Me.txt_total)
        Me.grp_orderForm.Controls.Add(Me.Label17)
        Me.grp_orderForm.Controls.Add(Me.txt_convertTotal)
        Me.grp_orderForm.Controls.Add(Me.Label16)
        Me.grp_orderForm.Controls.Add(Me.txt_payTerms)
        Me.grp_orderForm.Controls.Add(Me.drp_date)
        Me.grp_orderForm.Controls.Add(Me.Label7)
        Me.grp_orderForm.Controls.Add(Me.Label13)
        Me.grp_orderForm.Controls.Add(Me.txt_import)
        Me.grp_orderForm.Controls.Add(Me.Label14)
        Me.grp_orderForm.Controls.Add(Me.txt_exchangeRate)
        Me.grp_orderForm.Controls.Add(Me.Label15)
        Me.grp_orderForm.Controls.Add(Me.txt_commission)
        Me.grp_orderForm.Controls.Add(Me.Label12)
        Me.grp_orderForm.Controls.Add(Me.txt_shippingCost)
        Me.grp_orderForm.Controls.Add(Me.drp_deliveryDate)
        Me.grp_orderForm.Controls.Add(Me.Label11)
        Me.grp_orderForm.Controls.Add(Me.Label9)
        Me.grp_orderForm.Controls.Add(Me.txt_conPackReq)
        Me.grp_orderForm.Controls.Add(Me.Label10)
        Me.grp_orderForm.Controls.Add(Me.txt_contractPacking)
        Me.grp_orderForm.Controls.Add(Me.Label8)
        Me.grp_orderForm.Controls.Add(Me.txt_email)
        Me.grp_orderForm.Controls.Add(Me.Label6)
        Me.grp_orderForm.Controls.Add(Me.txt_name)
        Me.grp_orderForm.Controls.Add(Me.drp_buyers)
        Me.grp_orderForm.Controls.Add(Me.Label5)
        Me.grp_orderForm.Controls.Add(Me.Label4)
        Me.grp_orderForm.Controls.Add(Me.txt_purchaseNumber)
        Me.grp_orderForm.Controls.Add(Me.Label3)
        Me.grp_orderForm.Controls.Add(Me.Label2)
        Me.grp_orderForm.Controls.Add(Me.txt_supplier)
        Me.grp_orderForm.Controls.Add(Me.Label1)
        Me.grp_orderForm.Controls.Add(Me.txt_sageCode)
        Me.grp_orderForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grp_orderForm.Font = New System.Drawing.Font("Segoe UI", 7.25!)
        Me.grp_orderForm.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.grp_orderForm.HeaderText = "PRICECHECK TOILETRIES LTD - PURCHASE ORDER FROM"
        Me.grp_orderForm.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        Me.grp_orderForm.Location = New System.Drawing.Point(0, 0)
        Me.grp_orderForm.Name = "grp_orderForm"
        '
        '
        '
        Me.grp_orderForm.RootElement.Padding = New System.Windows.Forms.Padding(2, 18, 2, 2)
        Me.grp_orderForm.Size = New System.Drawing.Size(1838, 208)
        Me.grp_orderForm.TabIndex = 3
        Me.grp_orderForm.Text = "PRICECHECK TOILETRIES LTD - PURCHASE ORDER FROM"
        Me.grp_orderForm.Visible = False
        '
        'btn_submitOrder
        '
        Me.btn_submitOrder.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_submitOrder.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btn_submitOrder.Location = New System.Drawing.Point(1682, 140)
        Me.btn_submitOrder.Name = "btn_submitOrder"
        Me.btn_submitOrder.Size = New System.Drawing.Size(144, 25)
        Me.btn_submitOrder.TabIndex = 85
        Me.btn_submitOrder.Text = "Submit Order"
        '
        'btn_confirmDetails
        '
        Me.btn_confirmDetails.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_confirmDetails.BackgroundImage = Global.PO_Entry.My.Resources.Resources.tick
        Me.btn_confirmDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_confirmDetails.Location = New System.Drawing.Point(1788, 58)
        Me.btn_confirmDetails.Name = "btn_confirmDetails"
        Me.btn_confirmDetails.Size = New System.Drawing.Size(38, 36)
        Me.btn_confirmDetails.TabIndex = 84
        Me.btn_confirmDetails.TabStop = False
        Me.btn_confirmDetails.UseVisualStyleBackColor = True
        '
        'btn_GoBack
        '
        Me.btn_GoBack.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_GoBack.BackgroundImage = CType(resources.GetObject("btn_GoBack.BackgroundImage"), System.Drawing.Image)
        Me.btn_GoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_GoBack.Location = New System.Drawing.Point(1788, 19)
        Me.btn_GoBack.Name = "btn_GoBack"
        Me.btn_GoBack.Size = New System.Drawing.Size(38, 36)
        Me.btn_GoBack.TabIndex = 83
        Me.btn_GoBack.TabStop = False
        Me.btn_GoBack.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(1155, 84)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(117, 17)
        Me.Label19.TabIndex = 82
        Me.Label19.Text = "Notes/Instructions:"
        '
        'txt_notes
        '
        Me.txt_notes.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_notes.AutoSize = False
        Me.txt_notes.Font = New System.Drawing.Font("Segoe UI", 9.25!)
        Me.txt_notes.Location = New System.Drawing.Point(1158, 104)
        Me.txt_notes.Multiline = True
        Me.txt_notes.Name = "txt_notes"
        Me.txt_notes.Size = New System.Drawing.Size(232, 98)
        Me.txt_notes.TabIndex = 81
        '
        'lbl_vat
        '
        Me.lbl_vat.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_vat.AutoSize = True
        Me.lbl_vat.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.lbl_vat.Location = New System.Drawing.Point(1679, 188)
        Me.lbl_vat.Name = "lbl_vat"
        Me.lbl_vat.Size = New System.Drawing.Size(105, 13)
        Me.lbl_vat.TabIndex = 80
        Me.lbl_vat.Text = "VAT Included - 20%"
        Me.lbl_vat.Visible = False
        '
        'drp_currency
        '
        Me.drp_currency.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.drp_currency.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.drp_currency.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RadListDataItem1.Text = "SELECT"
        RadListDataItem1.TextWrap = True
        RadListDataItem2.Text = "£"
        RadListDataItem2.TextWrap = True
        RadListDataItem3.Text = "€"
        RadListDataItem3.TextWrap = True
        Me.drp_currency.Items.Add(RadListDataItem1)
        Me.drp_currency.Items.Add(RadListDataItem2)
        Me.drp_currency.Items.Add(RadListDataItem3)
        Me.drp_currency.Location = New System.Drawing.Point(631, 104)
        Me.drp_currency.Name = "drp_currency"
        Me.drp_currency.Size = New System.Drawing.Size(142, 23)
        Me.drp_currency.TabIndex = 79
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(1462, 144)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 17)
        Me.Label18.TabIndex = 78
        Me.Label18.Text = "Total Cost:"
        '
        'txt_total
        '
        Me.txt_total.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_total.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_total.Location = New System.Drawing.Point(1536, 140)
        Me.txt_total.Name = "txt_total"
        Me.txt_total.ReadOnly = True
        Me.txt_total.Size = New System.Drawing.Size(137, 25)
        Me.txt_total.TabIndex = 77
        '
        'Label17
        '
        Me.Label17.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(1408, 181)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(125, 17)
        Me.Label17.TabIndex = 76
        Me.Label17.Text = "Total Cost Currency:"
        '
        'txt_convertTotal
        '
        Me.txt_convertTotal.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_convertTotal.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_convertTotal.Location = New System.Drawing.Point(1536, 177)
        Me.txt_convertTotal.Name = "txt_convertTotal"
        Me.txt_convertTotal.ReadOnly = True
        Me.txt_convertTotal.Size = New System.Drawing.Size(137, 25)
        Me.txt_convertTotal.TabIndex = 75
        Me.txt_convertTotal.TabStop = False
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(824, 180)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(71, 17)
        Me.Label16.TabIndex = 74
        Me.Label16.Text = "Pay Terms:"
        '
        'txt_payTerms
        '
        Me.txt_payTerms.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_payTerms.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_payTerms.Location = New System.Drawing.Point(899, 177)
        Me.txt_payTerms.Name = "txt_payTerms"
        Me.txt_payTerms.Size = New System.Drawing.Size(226, 25)
        Me.txt_payTerms.TabIndex = 73
        '
        'drp_date
        '
        Me.drp_date.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.drp_date.Enabled = False
        Me.drp_date.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.drp_date.Location = New System.Drawing.Point(901, 33)
        Me.drp_date.Name = "drp_date"
        Me.drp_date.ReadOnly = True
        Me.drp_date.Size = New System.Drawing.Size(224, 23)
        Me.drp_date.TabIndex = 64
        Me.drp_date.TabStop = False
        Me.drp_date.Value = New Date(CType(0, Long))
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(569, 106)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 17)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Currency:"
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(815, 146)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 17)
        Me.Label13.TabIndex = 71
        Me.Label13.Text = "Import Duty:"
        '
        'txt_import
        '
        Me.txt_import.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_import.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_import.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_import.Location = New System.Drawing.Point(899, 143)
        Me.txt_import.Name = "txt_import"
        Me.txt_import.Size = New System.Drawing.Size(226, 25)
        Me.txt_import.TabIndex = 70
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(799, 109)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(96, 17)
        Me.Label14.TabIndex = 69
        Me.Label14.Text = "Exchange Rate:"
        '
        'txt_exchangeRate
        '
        Me.txt_exchangeRate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_exchangeRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_exchangeRate.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_exchangeRate.Location = New System.Drawing.Point(899, 106)
        Me.txt_exchangeRate.Name = "txt_exchangeRate"
        Me.txt_exchangeRate.ReadOnly = True
        Me.txt_exchangeRate.Size = New System.Drawing.Size(226, 25)
        Me.txt_exchangeRate.TabIndex = 68
        Me.txt_exchangeRate.TabStop = False
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(813, 72)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(82, 17)
        Me.Label15.TabIndex = 67
        Me.Label15.Text = "Commission:"
        '
        'txt_commission
        '
        Me.txt_commission.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_commission.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_commission.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_commission.Location = New System.Drawing.Point(899, 68)
        Me.txt_commission.Name = "txt_commission"
        Me.txt_commission.Size = New System.Drawing.Size(226, 25)
        Me.txt_commission.TabIndex = 66
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(533, 143)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(98, 17)
        Me.Label12.TabIndex = 65
        Me.Label12.Text = "Shipping Costs:"
        '
        'txt_shippingCost
        '
        Me.txt_shippingCost.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_shippingCost.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_shippingCost.Location = New System.Drawing.Point(631, 140)
        Me.txt_shippingCost.Name = "txt_shippingCost"
        Me.txt_shippingCost.Size = New System.Drawing.Size(142, 25)
        Me.txt_shippingCost.TabIndex = 63
        '
        'drp_deliveryDate
        '
        Me.drp_deliveryDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.drp_deliveryDate.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.drp_deliveryDate.Location = New System.Drawing.Point(631, 68)
        Me.drp_deliveryDate.Name = "drp_deliveryDate"
        Me.drp_deliveryDate.Size = New System.Drawing.Size(142, 23)
        Me.drp_deliveryDate.TabIndex = 62
        Me.drp_deliveryDate.TabStop = False
        Me.drp_deliveryDate.Value = New Date(CType(0, Long))
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(542, 71)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 17)
        Me.Label11.TabIndex = 61
        Me.Label11.Text = "Delivery Date:"
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(58, 180)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(144, 17)
        Me.Label9.TabIndex = 60
        Me.Label9.Text = "Contract Packing Req's:"
        '
        'txt_conPackReq
        '
        Me.txt_conPackReq.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_conPackReq.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_conPackReq.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_conPackReq.Location = New System.Drawing.Point(205, 177)
        Me.txt_conPackReq.Name = "txt_conPackReq"
        Me.txt_conPackReq.Size = New System.Drawing.Size(568, 25)
        Me.txt_conPackReq.TabIndex = 59
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(94, 143)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(108, 17)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "Contract Packing:"
        '
        'txt_contractPacking
        '
        Me.txt_contractPacking.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_contractPacking.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_contractPacking.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_contractPacking.Location = New System.Drawing.Point(205, 140)
        Me.txt_contractPacking.Name = "txt_contractPacking"
        Me.txt_contractPacking.Size = New System.Drawing.Size(235, 25)
        Me.txt_contractPacking.TabIndex = 57
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(161, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 17)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "Email:"
        '
        'txt_email
        '
        Me.txt_email.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_email.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_email.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_email.Location = New System.Drawing.Point(205, 106)
        Me.txt_email.Name = "txt_email"
        Me.txt_email.Size = New System.Drawing.Size(235, 25)
        Me.txt_email.TabIndex = 55
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(157, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 17)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Name:"
        '
        'txt_name
        '
        Me.txt_name.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_name.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_name.Location = New System.Drawing.Point(205, 69)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.Size = New System.Drawing.Size(235, 25)
        Me.txt_name.TabIndex = 53
        '
        'drp_buyers
        '
        Me.drp_buyers.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.drp_buyers.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.drp_buyers.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.drp_buyers.Location = New System.Drawing.Point(1536, 32)
        Me.drp_buyers.Name = "drp_buyers"
        Me.drp_buyers.Size = New System.Drawing.Size(137, 23)
        Me.drp_buyers.TabIndex = 52
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1489, 34)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 17)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Buyer:"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(1155, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 17)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "PO No:"
        '
        'txt_purchaseNumber
        '
        Me.txt_purchaseNumber.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_purchaseNumber.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_purchaseNumber.Location = New System.Drawing.Point(1208, 31)
        Me.txt_purchaseNumber.Name = "txt_purchaseNumber"
        Me.txt_purchaseNumber.Size = New System.Drawing.Size(182, 25)
        Me.txt_purchaseNumber.TabIndex = 49
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(857, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 17)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Date:"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(384, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 17)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Supplier:"
        '
        'txt_supplier
        '
        Me.txt_supplier.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_supplier.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_supplier.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_supplier.Location = New System.Drawing.Point(444, 31)
        Me.txt_supplier.Name = "txt_supplier"
        Me.txt_supplier.Size = New System.Drawing.Size(329, 25)
        Me.txt_supplier.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(127, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 17)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Sage Code:"
        '
        'txt_sageCode
        '
        Me.txt_sageCode.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txt_sageCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_sageCode.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sageCode.Location = New System.Drawing.Point(205, 31)
        Me.txt_sageCode.Name = "txt_sageCode"
        Me.txt_sageCode.Size = New System.Drawing.Size(135, 25)
        Me.txt_sageCode.TabIndex = 44
        '
        'SplitPanel2
        '
        Me.SplitPanel2.Controls.Add(Me.btn_viewPO)
        Me.SplitPanel2.Controls.Add(Me.btn_createPO)
        Me.SplitPanel2.Controls.Add(Me.flp_ordersLines)
        Me.SplitPanel2.Location = New System.Drawing.Point(0, 212)
        Me.SplitPanel2.Name = "SplitPanel2"
        '
        '
        '
        Me.SplitPanel2.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel2.Size = New System.Drawing.Size(1838, 654)
        Me.SplitPanel2.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, 0.2587007!)
        Me.SplitPanel2.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, 247)
        Me.SplitPanel2.TabIndex = 1
        Me.SplitPanel2.TabStop = False
        Me.SplitPanel2.Text = "SplitPanel2"
        '
        'flp_ordersLines
        '
        Me.flp_ordersLines.AutoScroll = True
        Me.flp_ordersLines.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flp_ordersLines.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.flp_ordersLines.Location = New System.Drawing.Point(0, 0)
        Me.flp_ordersLines.Name = "flp_ordersLines"
        Me.flp_ordersLines.Size = New System.Drawing.Size(1838, 654)
        Me.flp_ordersLines.TabIndex = 2
        '
        'btn_viewPO
        '
        Me.btn_viewPO.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.btn_viewPO.Location = New System.Drawing.Point(1012, 247)
        Me.btn_viewPO.Name = "btn_viewPO"
        Me.btn_viewPO.Size = New System.Drawing.Size(221, 93)
        Me.btn_viewPO.TabIndex = 1
        Me.btn_viewPO.Text = "View Submitted PO's"
        '
        'btn_createPO
        '
        Me.btn_createPO.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.btn_createPO.Location = New System.Drawing.Point(632, 247)
        Me.btn_createPO.Name = "btn_createPO"
        Me.btn_createPO.Size = New System.Drawing.Size(192, 93)
        Me.btn_createPO.TabIndex = 0
        Me.btn_createPO.Text = "Create New PO"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1838, 866)
        Me.Controls.Add(Me.RadSplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Purchase Order Entry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadSplitContainer1.ResumeLayout(False)
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitPanel1.ResumeLayout(False)
        CType(Me.grp_orderForm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_orderForm.ResumeLayout(False)
        Me.grp_orderForm.PerformLayout()
        CType(Me.btn_submitOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_notes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.drp_currency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.drp_date, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.drp_deliveryDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.drp_buyers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitPanel2.ResumeLayout(False)
        CType(Me.btn_viewPO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_createPO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadSplitContainer1 As Telerik.WinControls.UI.RadSplitContainer
    Friend WithEvents SplitPanel1 As Telerik.WinControls.UI.SplitPanel
    Friend WithEvents SplitPanel2 As Telerik.WinControls.UI.SplitPanel
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents btn_viewPO As Telerik.WinControls.UI.RadButton
    Friend WithEvents btn_createPO As Telerik.WinControls.UI.RadButton
    Friend WithEvents grp_orderForm As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txt_notes As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents lbl_vat As System.Windows.Forms.Label
    Friend WithEvents drp_currency As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_total As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_convertTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_payTerms As System.Windows.Forms.TextBox
    Friend WithEvents drp_date As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt_import As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_exchangeRate As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_commission As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_shippingCost As System.Windows.Forms.TextBox
    Friend WithEvents drp_deliveryDate As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_conPackReq As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_contractPacking As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_email As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_name As System.Windows.Forms.TextBox
    Friend WithEvents drp_buyers As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_purchaseNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_supplier As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_sageCode As System.Windows.Forms.TextBox
    Friend WithEvents btn_GoBack As System.Windows.Forms.Button
    Friend WithEvents btn_confirmDetails As System.Windows.Forms.Button
    Friend WithEvents btn_submitOrder As Telerik.WinControls.UI.RadButton
    Friend WithEvents flp_ordersLines As System.Windows.Forms.FlowLayoutPanel

End Class
