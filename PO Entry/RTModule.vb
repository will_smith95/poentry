﻿Imports System.Data.SqlClient
Imports System.DirectoryServices
Imports System.Text
Imports System.Security.Principal
'Imports Microsoft.Office.Interop.Outlook
Imports System.Web.Services.Protocols
Imports System.IO
Imports EnquiryConsole.ReportExecution2005
Imports System.Threading
Imports System.Net.Mail
Imports System.DirectoryServices.AccountManagement
Imports System.Text.RegularExpressions

Module RTModule

    Public Sub RunReportFromSSRS(ByVal reportLocation As String, ByVal reportName As String, ByVal reportPathName As String, Optional parameterName As String = "", Optional parameter As String = "")
        Dim rs As New ReportExecutionService()
        rs.Credentials = System.Net.CredentialCache.DefaultCredentials
        rs.Url = "http://ptlsage/ReportServer/ReportExecution2005.asmx"

        ' Render arguments
        Dim result As Byte() = Nothing
        Dim reportPath As String = "/" + reportPathName + "/" + reportLocation + ""
        Dim format As String = "PDF"
        Dim historyID As String = Nothing
        Dim devInfo As String = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>"
        Dim parameters(0) As ParameterValue
        ' Prepare report parameter.
        If parameterName <> "" Then
            parameters(0) = New ParameterValue()
            parameters(0).Name = parameterName
            parameters(0).Value = parameter
        End If

        Dim credentials As DataSourceCredentials() = Nothing
        Dim showHideToggle As String = Nothing
        Dim encoding As String = ""
        Dim mimeType As String = ""
        Dim warnings As Warning() = Nothing
        Dim reportHistoryParameters As ParameterValue() = Nothing
        Dim streamIDs As String() = Nothing

        Dim execInfo As New ExecutionInfo
        Dim execHeader As New ExecutionHeader()
        Dim SessionId As String
        Dim extension As String = ""

        rs.ExecutionHeaderValue = execHeader

        execInfo = rs.LoadReport(reportPath, historyID)
        If parameterName <> "" Then
            rs.SetExecutionParameters(parameters, "en-gb")
        End If


        SessionId = rs.ExecutionHeaderValue.ExecutionID
        Console.WriteLine("SessionID: {0}", rs.ExecutionHeaderValue.ExecutionID)
        Try
            result = rs.Render(format, devInfo, mimeType, encoding, warnings, streamIDs)
            execInfo = rs.GetExecutionInfo()
            Console.WriteLine("Execution date and time: {0}", execInfo.ExecutionDateTime)
        Catch e As SoapException
            Console.WriteLine(e.Detail.OuterXml)
        End Try
        Try
            If My.Computer.FileSystem.DirectoryExists("U:\EnquiryConsoleSettings") Then

                Dim stream As FileStream = File.Create("U:\EnquiryConsoleSettings\" + reportName + ".pdf", result.Length)
                Console.WriteLine("File created.")
                stream.Write(result, 0, result.Length)
                Console.WriteLine("Result written to the file.")
                stream.Close()
            Else
                My.Computer.FileSystem.CreateDirectory("U:\EnquiryConsoleSettings")
                Dim stream As FileStream = File.Create("U:\EnquiryConsoleSettings\" + reportName + ".pdf", result.Length)
                Console.WriteLine("File created.")
                stream.Write(result, 0, result.Length)
                Console.WriteLine("Result written to the file.")
                stream.Close()
            End If
        Catch e As System.Exception
            Console.WriteLine(e.Message)
            sendCaughtError(e)
        End Try
    End Sub

    Function getDataSetFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)
            connection.Close()
        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
            connection.Close()
        End Try

        Return tempDataset

    End Function
    Function getDataSetFromPTLSAGECustomQuery(ByVal query As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = query
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)
            connection.Close()
        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
            connection.Close()
        End Try

        Return tempDataset

    End Function
    Function executeNonQueryFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "", _
                                   Optional param7Name As String = "", _
                                   Optional param7Val As String = "", _
                                   Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)

        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If

            End If
            command.ExecuteNonQuery()
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return False
        End Try
        Return True
    End Function

    Function executeScalarFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "", _
                                   Optional param7Name As String = "", _
                                   Optional param7Val As String = "", _
                                   Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Dim result As Int32 = 0
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If
            End If
            result = Convert.ToInt32(command.ExecuteScalar())
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return -1
        End Try
        param1Name = ""
        param1Val = ""
        param2Name = ""
        param2Val = ""
        param3Name = ""
        param3Val = ""
        storedProcedure = ""

        Return result
    End Function

    Function executeNonQueryFromPTLSAGECustomQuery(ByVal query As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "", _
                                   Optional param7Name As String = "", _
                                   Optional param7Val As String = "", _
                                   Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = query
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If

            End If
            command.ExecuteNonQuery()
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return False
        End Try
        Return True
    End Function

    Public Function IsInGroup(ByVal GroupName As String, ByVal UserName As String)

        Dim MyPrincipal As New System.Security.Principal.WindowsPrincipal _
     (New System.Security.Principal.WindowsIdentity(UserName))
        Dim blnValid1 As Boolean = MyPrincipal.IsInRole(GroupName)
        If blnValid1 = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetGroups(userName As String) As List(Of String)

        Dim result As New List(Of String)
        Dim wi As WindowsIdentity = New WindowsIdentity(userName)

        For Each group As IdentityReference In wi.Groups
            Try
                result.Add(group.Translate(GetType(NTAccount)).ToString())
            Catch ex As System.Exception
            End Try
        Next

        result.Sort()
        Return result
    End Function

    Public Sub openOutlookClient(ByVal recipent As String, ByVal subject As String, _
                                 ByVal body As String, ByVal attachment As String)

    End Sub
    Public Function db_conx(ByVal hash As String)
        Dim returnValue As String = ""
        If hash = "034cdaf241d87a07c5e5d47261a6f2de" Then
            returnValue = "Data Source=PTLSAGE;Initial Catalog=MPSPriceCheck;User ID=admin;Password=barry250372;"
            Return returnValue
        End If
        Return returnValue
    End Function
    Public Function db_conx2(ByVal hash As String)
        Dim returnValue As String = ""
        If hash = "034cdaf241d87a07c5e5d47261a6f2de" Then
            returnValue = "Data Source=PTLSAGE;Initial Catalog=Analysis;User ID=admin;Password=barry250372;"
            Return returnValue
        End If
        Return returnValue
    End Function

    Public Function sanitize(ByVal s As String)
        s = s.Replace("'", "")
        s = s.Replace("--", "")
        s = s.Replace("/*", "")
        s = s.Replace("*/", "")
        s = s.Replace("!", "")
        s = s.Replace("Ã±", "n")
        s = s.Replace("Ã¡", "a")

        Return s
    End Function
    Function RandomString(r As Random)
        Dim s As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        Dim sb As New StringBuilder
        Dim cnt As Integer = r.Next(15, 33)
        For i As Integer = 1 To cnt
            Dim idx As Integer = r.Next(0, s.Length)
            sb.Append(s.Substring(idx, 1))
        Next
        Return sb.ToString()
    End Function
    Public Sub sendCaughtError(ByVal ex As System.Exception)
        Dim exc As String = ""
        Try
            Dim SmtpServer As New SmtpClient()
            SmtpServer.Credentials = New Net.NetworkCredential("ptl.smtp.rt@gmail.com", "74242852")
            SmtpServer.Port = 587
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.EnableSsl = True
            Dim omail As New MailMessage()
            omail.From = New MailAddress("VB.APPLICATION@pricecheck.uk.com", "Enquiry Console-CAUGHT ERROR", System.Text.Encoding.UTF8)
            omail.Subject = "EnquiryConsole Caught Error"
            omail.To.Add("ITSupport@pricecheck.uk.com")
            Dim st As New StackTrace(ex, True)


            omail.Body = "The following errors occured from user:" + UserPrincipal.Current.UserPrincipalName & vbCr & vbCr & vbCr + ex.ToString

            SmtpServer.Send(omail) ', Nothing)
            omail.Dispose()
            Console.WriteLine("Error Successfully Sent")
        Catch ext As System.Exception
            exc = ext.ToString
            Console.WriteLine(exc)
        End Try
    End Sub
    'End Try
End Module
