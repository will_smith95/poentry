﻿Imports System.IO
Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient
Imports System.Globalization
Imports System.Windows.Forms

Public Class Form1

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Loader()
    End Sub

    Private Sub Loader()

        AddHandler btn_createPO.Click, AddressOf CreateForm
        AddHandler btn_GoBack.Click, AddressOf GoBack
        AddHandler btn_confirmDetails.Click, AddressOf ConfirmOrder
        'AddHandler btn_viewPO.Click, AddressOf ShowForms
        AddHandler txt_sageCode.TextChanged, AddressOf supplierSearch
        AddHandler drp_currency.SelectedIndexChanged, AddressOf GetExchangeRate
        AddHandler txt_total.TextChanged, AddressOf GetCurrencyCost
        AddHandler drp_currency.SelectedIndexChanged, AddressOf GetCurrencyCost

        AutoCompleter()
        GetBuyersDropdown()
    End Sub

    Dim supDT As New DataTable
    Dim RunningTotal As Double = 0
    Dim gettingTotal As Boolean = False

#Region "--------- Useful Functions ---------"
    Public Function sanitize(ByVal s As String)
        s = s.Replace("'", "")
        s = s.Replace("--", "")
        s = s.Replace("/*", "")
        s = s.Replace("*/", "")
        s = s.Replace(";", "")
        Return s
    End Function

#End Region

#Region " --------------------------- Database Functions -------------------------------"

    Function getDataSetFromPTLSAGE(ByVal storedProcedure As String, _
                                  ByVal connectionString As String, _
                                  Optional param1Name As String = "", _
                                  Optional param1Val As String = "", _
                                  Optional param2Name As String = "", _
                                  Optional param2Val As String = "", _
                                  Optional param3Name As String = "", _
                                  Optional param3Val As String = "", _
                                  Optional param4Name As String = "", _
                                  Optional param4Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)
            connection.Close()
        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
            connection.Close()
        End Try

        Return tempDataset

    End Function
    Function getDataSetFromPTLSAGECustomQuery(ByVal query As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = query
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)
            connection.Close()
        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
            connection.Close()
        End Try

        Return tempDataset

    End Function
    Function executeNonQueryFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "", _
                                   Optional param7Name As String = "", _
                                   Optional param7Val As String = "", _
                                   Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)

        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If

            End If
            command.ExecuteNonQuery()
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return False
        End Try
        Return True
    End Function

    Function executeScalarFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "", _
                                   Optional param7Name As String = "", _
                                   Optional param7Val As String = "", _
                                   Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Dim result As Int32 = 0
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If
            End If
            result = Convert.ToInt32(command.ExecuteScalar())
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return -1
        End Try
        param1Name = ""
        param1Val = ""
        param2Name = ""
        param2Val = ""
        param3Name = ""
        param3Val = ""
        storedProcedure = ""

        Return result
    End Function

    Function executeNonQueryFromPTLSAGECustomQuery(ByVal query As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "", _
                                   Optional param7Name As String = "", _
                                   Optional param7Val As String = "", _
                                   Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = query
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If

            End If
            command.ExecuteNonQuery()
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return False
        End Try
        Return True
    End Function

    Public Function db_conx(ByVal hash As String)
        Dim returnValue As String = ""
        If hash = "034cdaf241d87a07c5e5d47261a6f2de" Then
            returnValue = "Data Source=PTLSAGE;Initial Catalog=MPSPriceCheck;User ID=admin;Password=barry250372;"
            Return returnValue
        End If
        Return returnValue
    End Function

#End Region

    Private Sub PushSupplier(sender As Object, e As KeyEventArgs) Handles txt_supplier.KeyDown

        If e.KeyCode = Keys.Enter Then

            Dim globalCode As String = ""
            Dim ds As New DataSet
            Dim DT As New DataTable

            ds = getDataSetFromPTLSAGECustomQuery("SELECT supplier from live.scheme.plsuppm where name = '" + txt_supplier.Text.Trim + "'", db_conx(My.Settings.ConnectionString))
            DT = ds.Tables(0)

            globalCode = ""

            If DT.Rows.Count > 0 Then

                globalCode = DT.Rows(0)(0).ToString.Trim
            End If

            If globalCode <> "" Then

                txt_sageCode.Text = globalCode
            End If

            e.SuppressKeyPress = True
        End If
    End Sub

    Public Sub GetTotal(ByVal sender As Object, ByVal e As KeyEventArgs)

        If e.KeyCode = Keys.Enter Then

            e.SuppressKeyPress = True

            gettingTotal = True
            CreateOrderForm()
        End If
    End Sub

    Public Sub newLine(ByVal sender As Object, ByVal e As KeyEventArgs)

        If e.KeyCode = Keys.Escape Then

            e.SuppressKeyPress = True

            CreateOrderForm()
        End If
    End Sub

    Private Sub CreateForm()

        grp_orderForm.Visible = True
        btn_createPO.Visible = False
        btn_viewPO.Visible = False
        btn_confirmDetails.Visible = True
    End Sub

    Private Sub Clear()

        'Clears the binding of info from table
        txt_email.DataBindings.Clear()
        txt_name.DataBindings.Clear()
        txt_payTerms.DataBindings.Clear()
        txt_supplier.DataBindings.Clear()

        'Clears the Textboxes
        txt_supplier.Text = ""
        txt_email.Text = ""
        txt_name.Text = ""
        txt_payTerms.Text = ""
        txt_exchangeRate.Text = ""
        drp_currency.SelectedIndex = 0
        drp_buyers.SelectedIndex = 0
        drp_deliveryDate.SetToNullValue()
        txt_total.Text = ""

        'Clears FlowPanel
        flp_ordersLines.Controls.Clear()
    End Sub

    Public Sub AutoCompleter()

        Dim ds As New DataSet

        ds = getDataSetFromPTLSAGECustomQuery("SELECT supplier, name from live.scheme.plsuppm", db_conx(My.Settings.ConnectionString))

        Dim codeCol As New AutoCompleteStringCollection
        Dim supCol As New AutoCompleteStringCollection
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            codeCol.Add(ds.Tables(0).Rows(i)("supplier").ToString().ToUpper)
            supCol.Add(ds.Tables(0).Rows(i)("name").ToString().ToUpper)
        Next

        txt_sageCode.AutoCompleteSource = AutoCompleteSource.CustomSource
        txt_sageCode.AutoCompleteCustomSource = codeCol
        txt_sageCode.AutoCompleteMode = AutoCompleteMode.Suggest

        txt_supplier.AutoCompleteSource = AutoCompleteSource.CustomSource
        txt_supplier.AutoCompleteCustomSource = supCol
        txt_supplier.AutoCompleteMode = AutoCompleteMode.Suggest

    End Sub

    Private Sub supplierSearch()

        Clear()

        Dim search As String

        search = txt_sageCode.Text.Trim

        Dim ds = getDataSetFromPTLSAGECustomQuery("select supplier, name, pay_terms, sup.currency, sup.email as secondEmail, em.email as email, em.notes from live.scheme.plsuppm as sup left join MPSPriceCheck.dbo.email_contacts as em " & _
                                                  "on em.code = sup.supplier COLLATE SQL_Latin1_General_CP1_CI_AS WHERE supplier = '" + sanitize(search) + "'",
                                                  db_conx(My.Settings.ConnectionString))

        supDT = ds.tables(0)


        If supDT.Rows.Count <> 0 Then

            BindingSource1.DataSource = supDT
            controlBinding(BindingSource1)

            If supDT.Rows(0)("currency").ToString.Trim = "EUR" Then

                drp_currency.SelectedIndex = 2
            End If
        End If
    End Sub

    Private Sub controlBinding(ByVal bs As BindingSource)

        Clear()

        'Adds the data specified onto the textbox ("type", datasource, "field_in_database", T/F for boolean recognition)
        txt_supplier.DataBindings.Add(New Binding("Text", bs, "name", True))

        If supDT.Rows(0)("email").ToString.Trim <> "" Then
            txt_email.DataBindings.Add(New Binding("Text", bs, "email", True))
        Else
            txt_email.DataBindings.Add(New Binding("Text", bs, "secondEmail", True))
        End If

        txt_name.DataBindings.Add(New Binding("Text", bs, "notes", True))
        txt_payTerms.DataBindings.Add(New Binding("Text", bs, "pay_terms", True))

        txt_supplier.Text = txt_supplier.Text.Trim

    End Sub

    Private Sub CreateOrderForm()

        If gettingTotal = False Then

            Dim line As New POLines
            flp_ordersLines.Controls.Add(line)
            flp_ordersLines.Dock = DockStyle.Top

        Else

            Dim line As POLines

            RunningTotal = 0

            For Each line In flp_ordersLines.Controls

                If (IsNumeric(line.txt_totalCost.Text.Trim) And line.txt_totalCost.Text.Trim <> "") Then
                    RunningTotal += line.txt_totalCost.Text
                End If
            Next

            txt_total.Text = RunningTotal.ToString("N")

        End If

        gettingTotal = False
    End Sub

    Private Sub ConfirmOrder()

        Dim Errors As String = ValidateForm()

        If Errors = "" Then
            Dim result = MessageBox.Show("Are You Sure These Details Are Correct?", "Confirm Supplier Details", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If result = DialogResult.Yes Then

                btn_confirmDetails.Visible = False
                flp_ordersLines.Visible = True
                CreateOrderForm()
            End If
        End If

    End Sub

    Private Function ValidateForm()
        Dim errors As String = ""

        Return errors
    End Function

    Private Sub GoBack()

        Dim result = MessageBox.Show("Are You Sure You Want To Delete This Form?" + vbCr + vbCr + "Any Progress Will Be Lost!", "Delete Form", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If result = DialogResult.Yes Then

            Clear()
            txt_sageCode.Text = ""
            grp_orderForm.Visible = False
            btn_createPO.Visible = True
            btn_viewPO.Visible = True
        End If

    End Sub

    Private Sub GetBuyersDropdown()

        Dim Buyers As DataSet = getDataSetFromPTLSAGE("ACT_BuyingActive_Users", db_conx(My.Settings.ConnectionString))

        If Buyers.Tables(0).Rows.Count > 0 Then
            Try

                Dim row1 As DataRow = Buyers.Tables(0).NewRow
                row1("Username") = "SELECT"
                Buyers.Tables(0).Rows.InsertAt(row1, 0)

                drp_buyers.DataSource = Buyers.Tables(0)
                drp_buyers.DisplayMember = "Username"
                drp_buyers.ValueMember = "Username"

            Catch ex As Exception
                MsgBox("There's Been An Error Adding Buyers. " + vbCr + vbCr + ex.ToString)
            End Try

        End If

        drp_date.Text = Today()

    End Sub

    Private Sub GetExchangeRate()

        If drp_currency.Text.Trim = "€" Then
            Dim exchange As DataSet = getDataSetFromPTLSAGE("WebQuoteEuroConvert", db_conx(My.Settings.ConnectionString), "@sterlingValue", 1)

            txt_exchangeRate.Text = exchange.Tables(0).Rows(0)(0)
        ElseIf drp_currency.Text.Trim = "£" Then

            txt_exchangeRate.Text = "N/A"

        End If
    End Sub

    Private Sub GetCurrencyCost()

        txt_convertTotal.Text = ""
        lbl_vat.Visible = False

        If IsNumeric(txt_total.Text) Then

            If drp_currency.Text.Trim = "€" Or drp_currency.Text.Trim = "$" Then

                Dim converted As Double
                converted = (txt_total.Text * txt_exchangeRate.Text)
                txt_convertTotal.Text = drp_currency.Text.Trim + converted.ToString("N")

            Else

                Dim converted As Double
                converted = txt_total.Text * 1.2
                txt_convertTotal.Text = "£" + converted.ToString("N")
                lbl_vat.Visible = True
            End If
        End If
    End Sub

End Class
