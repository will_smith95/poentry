﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class POLines
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RadSplitContainer1 = New Telerik.WinControls.UI.RadSplitContainer()
        Me.SplitPanel1 = New Telerik.WinControls.UI.SplitPanel()
        Me.txt_outerBarcode = New PO_Entry.PlaceHolderTextBox()
        Me.txt_supplierCode = New PO_Entry.PlaceHolderTextBox()
        Me.txt_languages = New PO_Entry.PlaceHolderTextBox()
        Me.txt_code = New PO_Entry.PlaceHolderTextBox()
        Me.txt_gslCode = New PO_Entry.PlaceHolderTextBox()
        Me.txt_barcode = New PO_Entry.PlaceHolderTextBox()
        Me.txt_msds = New PO_Entry.PlaceHolderTextBox()
        Me.txt_exp = New PO_Entry.PlaceHolderTextBox()
        Me.txt_repeatable = New PO_Entry.PlaceHolderTextBox()
        Me.txt_webRestrict = New PO_Entry.PlaceHolderTextBox()
        Me.txt_mults = New PO_Entry.PlaceHolderTextBox()
        Me.txt_outer = New PO_Entry.PlaceHolderTextBox()
        Me.txt_inner = New PO_Entry.PlaceHolderTextBox()
        Me.txt_pallet = New PO_Entry.PlaceHolderTextBox()
        Me.txt_physicalOrder = New PO_Entry.PlaceHolderTextBox()
        Me.txt_oldAdmin = New PO_Entry.PlaceHolderTextBox()
        Me.txt_totalCost = New PO_Entry.PlaceHolderTextBox()
        Me.txt_newAdmin = New PO_Entry.PlaceHolderTextBox()
        Me.txt_volPrice = New PO_Entry.PlaceHolderTextBox()
        Me.txt_wholesale = New PO_Entry.PlaceHolderTextBox()
        Me.txt_com = New PO_Entry.PlaceHolderTextBox()
        Me.txt_rsp = New PO_Entry.PlaceHolderTextBox()
        Me.txt_fullDesc = New PO_Entry.PlaceHolderTextBox()
        Me.txt_costCurrency = New PO_Entry.PlaceHolderTextBox()
        Me.PlaceHolderTextBox1 = New PO_Entry.PlaceHolderTextBox()
        Me.txt_product = New PO_Entry.PlaceHolderTextBox()
        Me.txt_quantity = New PO_Entry.PlaceHolderTextBox()
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadSplitContainer1.SuspendLayout()
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'RadSplitContainer1
        '
        Me.RadSplitContainer1.Controls.Add(Me.SplitPanel1)
        Me.RadSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadSplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.RadSplitContainer1.Name = "RadSplitContainer1"
        Me.RadSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '
        '
        Me.RadSplitContainer1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadSplitContainer1.Size = New System.Drawing.Size(1814, 89)
        Me.RadSplitContainer1.SplitterWidth = 4
        Me.RadSplitContainer1.TabIndex = 0
        Me.RadSplitContainer1.TabStop = False
        Me.RadSplitContainer1.Text = "RadSplitContainer1"
        '
        'SplitPanel1
        '
        Me.SplitPanel1.Controls.Add(Me.txt_outerBarcode)
        Me.SplitPanel1.Controls.Add(Me.txt_supplierCode)
        Me.SplitPanel1.Controls.Add(Me.txt_languages)
        Me.SplitPanel1.Controls.Add(Me.txt_code)
        Me.SplitPanel1.Controls.Add(Me.txt_gslCode)
        Me.SplitPanel1.Controls.Add(Me.txt_barcode)
        Me.SplitPanel1.Controls.Add(Me.txt_msds)
        Me.SplitPanel1.Controls.Add(Me.txt_exp)
        Me.SplitPanel1.Controls.Add(Me.txt_repeatable)
        Me.SplitPanel1.Controls.Add(Me.txt_webRestrict)
        Me.SplitPanel1.Controls.Add(Me.txt_mults)
        Me.SplitPanel1.Controls.Add(Me.txt_outer)
        Me.SplitPanel1.Controls.Add(Me.txt_inner)
        Me.SplitPanel1.Controls.Add(Me.txt_pallet)
        Me.SplitPanel1.Controls.Add(Me.txt_physicalOrder)
        Me.SplitPanel1.Controls.Add(Me.txt_oldAdmin)
        Me.SplitPanel1.Controls.Add(Me.txt_totalCost)
        Me.SplitPanel1.Controls.Add(Me.txt_newAdmin)
        Me.SplitPanel1.Controls.Add(Me.txt_volPrice)
        Me.SplitPanel1.Controls.Add(Me.txt_wholesale)
        Me.SplitPanel1.Controls.Add(Me.txt_com)
        Me.SplitPanel1.Controls.Add(Me.txt_rsp)
        Me.SplitPanel1.Controls.Add(Me.txt_fullDesc)
        Me.SplitPanel1.Controls.Add(Me.txt_costCurrency)
        Me.SplitPanel1.Controls.Add(Me.PlaceHolderTextBox1)
        Me.SplitPanel1.Controls.Add(Me.txt_product)
        Me.SplitPanel1.Controls.Add(Me.txt_quantity)
        Me.SplitPanel1.Location = New System.Drawing.Point(0, 0)
        Me.SplitPanel1.Name = "SplitPanel1"
        '
        '
        '
        Me.SplitPanel1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel1.Size = New System.Drawing.Size(1814, 89)
        Me.SplitPanel1.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, -0.02586207!)
        Me.SplitPanel1.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, -3)
        Me.SplitPanel1.TabIndex = 0
        Me.SplitPanel1.TabStop = False
        Me.SplitPanel1.Text = "SplitPanel1"
        '
        'txt_outerBarcode
        '
        Me.txt_outerBarcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_outerBarcode.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_outerBarcode.ForeColor = System.Drawing.Color.Gray
        Me.txt_outerBarcode.Location = New System.Drawing.Point(914, 55)
        Me.txt_outerBarcode.Name = "txt_outerBarcode"
        Me.txt_outerBarcode.PlaceHolderText = "Outer Barcode"
        Me.txt_outerBarcode.Size = New System.Drawing.Size(119, 23)
        Me.txt_outerBarcode.TabIndex = 44
        Me.txt_outerBarcode.Text = "OUTER BARCODE"
        '
        'txt_supplierCode
        '
        Me.txt_supplierCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_supplierCode.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_supplierCode.ForeColor = System.Drawing.Color.Gray
        Me.txt_supplierCode.Location = New System.Drawing.Point(1551, 55)
        Me.txt_supplierCode.Name = "txt_supplierCode"
        Me.txt_supplierCode.PlaceHolderText = "Supplier Product Code"
        Me.txt_supplierCode.Size = New System.Drawing.Size(107, 23)
        Me.txt_supplierCode.TabIndex = 43
        Me.txt_supplierCode.Text = "SUPPLIER CODE"
        '
        'txt_languages
        '
        Me.txt_languages.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_languages.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_languages.ForeColor = System.Drawing.Color.Gray
        Me.txt_languages.Location = New System.Drawing.Point(1233, 55)
        Me.txt_languages.Name = "txt_languages"
        Me.txt_languages.PlaceHolderText = "Languages"
        Me.txt_languages.Size = New System.Drawing.Size(210, 23)
        Me.txt_languages.TabIndex = 42
        Me.txt_languages.Text = "LANGUAGES"
        '
        'txt_code
        '
        Me.txt_code.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_code.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_code.ForeColor = System.Drawing.Color.Gray
        Me.txt_code.Location = New System.Drawing.Point(1124, 55)
        Me.txt_code.Name = "txt_code"
        Me.txt_code.PlaceHolderText = "Q/G Code"
        Me.txt_code.Size = New System.Drawing.Size(99, 23)
        Me.txt_code.TabIndex = 41
        Me.txt_code.Text = "Q/G CODE"
        '
        'txt_gslCode
        '
        Me.txt_gslCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_gslCode.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_gslCode.ForeColor = System.Drawing.Color.Gray
        Me.txt_gslCode.Location = New System.Drawing.Point(1453, 55)
        Me.txt_gslCode.Name = "txt_gslCode"
        Me.txt_gslCode.PlaceHolderText = "GSL/PL Code"
        Me.txt_gslCode.Size = New System.Drawing.Size(89, 23)
        Me.txt_gslCode.TabIndex = 40
        Me.txt_gslCode.Text = "GSL/PL CODE"
        '
        'txt_barcode
        '
        Me.txt_barcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_barcode.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_barcode.ForeColor = System.Drawing.Color.Gray
        Me.txt_barcode.Location = New System.Drawing.Point(786, 55)
        Me.txt_barcode.Name = "txt_barcode"
        Me.txt_barcode.PlaceHolderText = "Barcode"
        Me.txt_barcode.Size = New System.Drawing.Size(119, 23)
        Me.txt_barcode.TabIndex = 39
        Me.txt_barcode.Text = "BARCODE"
        '
        'txt_msds
        '
        Me.txt_msds.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_msds.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_msds.ForeColor = System.Drawing.Color.Gray
        Me.txt_msds.Location = New System.Drawing.Point(724, 55)
        Me.txt_msds.Name = "txt_msds"
        Me.txt_msds.PlaceHolderText = "MSDS?"
        Me.txt_msds.Size = New System.Drawing.Size(54, 23)
        Me.txt_msds.TabIndex = 38
        Me.txt_msds.Text = "MSDS?"
        '
        'txt_exp
        '
        Me.txt_exp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_exp.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_exp.ForeColor = System.Drawing.Color.Gray
        Me.txt_exp.Location = New System.Drawing.Point(1044, 55)
        Me.txt_exp.Name = "txt_exp"
        Me.txt_exp.PlaceHolderText = "EXP"
        Me.txt_exp.Size = New System.Drawing.Size(70, 23)
        Me.txt_exp.TabIndex = 37
        Me.txt_exp.Text = "EXP"
        '
        'txt_repeatable
        '
        Me.txt_repeatable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_repeatable.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_repeatable.ForeColor = System.Drawing.Color.Gray
        Me.txt_repeatable.Location = New System.Drawing.Point(578, 55)
        Me.txt_repeatable.Name = "txt_repeatable"
        Me.txt_repeatable.PlaceHolderText = "Repeatable?"
        Me.txt_repeatable.Size = New System.Drawing.Size(80, 23)
        Me.txt_repeatable.TabIndex = 36
        Me.txt_repeatable.Text = "REPEATABLE?"
        '
        'txt_webRestrict
        '
        Me.txt_webRestrict.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_webRestrict.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_webRestrict.ForeColor = System.Drawing.Color.Gray
        Me.txt_webRestrict.Location = New System.Drawing.Point(463, 55)
        Me.txt_webRestrict.Name = "txt_webRestrict"
        Me.txt_webRestrict.PlaceHolderText = "Web Restricted?"
        Me.txt_webRestrict.Size = New System.Drawing.Size(107, 23)
        Me.txt_webRestrict.TabIndex = 35
        Me.txt_webRestrict.Text = "WEB RESTRICTED?"
        '
        'txt_mults
        '
        Me.txt_mults.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_mults.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_mults.ForeColor = System.Drawing.Color.Gray
        Me.txt_mults.Location = New System.Drawing.Point(664, 55)
        Me.txt_mults.Name = "txt_mults"
        Me.txt_mults.PlaceHolderText = "Mults?"
        Me.txt_mults.Size = New System.Drawing.Size(51, 23)
        Me.txt_mults.TabIndex = 34
        Me.txt_mults.Text = "MULTS?"
        '
        'txt_outer
        '
        Me.txt_outer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_outer.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_outer.ForeColor = System.Drawing.Color.Gray
        Me.txt_outer.Location = New System.Drawing.Point(248, 55)
        Me.txt_outer.Name = "txt_outer"
        Me.txt_outer.PlaceHolderText = "Outer"
        Me.txt_outer.Size = New System.Drawing.Size(100, 23)
        Me.txt_outer.TabIndex = 33
        Me.txt_outer.Text = "OUTER"
        '
        'txt_inner
        '
        Me.txt_inner.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_inner.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_inner.ForeColor = System.Drawing.Color.Gray
        Me.txt_inner.Location = New System.Drawing.Point(140, 55)
        Me.txt_inner.Name = "txt_inner"
        Me.txt_inner.PlaceHolderText = "Inner"
        Me.txt_inner.Size = New System.Drawing.Size(100, 23)
        Me.txt_inner.TabIndex = 32
        Me.txt_inner.Text = "INNER"
        '
        'txt_pallet
        '
        Me.txt_pallet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_pallet.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_pallet.ForeColor = System.Drawing.Color.Gray
        Me.txt_pallet.Location = New System.Drawing.Point(356, 55)
        Me.txt_pallet.Name = "txt_pallet"
        Me.txt_pallet.PlaceHolderText = "Pallet"
        Me.txt_pallet.Size = New System.Drawing.Size(100, 23)
        Me.txt_pallet.TabIndex = 31
        Me.txt_pallet.Text = "PALLET"
        '
        'txt_physicalOrder
        '
        Me.txt_physicalOrder.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_physicalOrder.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_physicalOrder.ForeColor = System.Drawing.Color.Gray
        Me.txt_physicalOrder.Location = New System.Drawing.Point(1651, 15)
        Me.txt_physicalOrder.Name = "txt_physicalOrder"
        Me.txt_physicalOrder.PlaceHolderText = "Physical + On Order"
        Me.txt_physicalOrder.Size = New System.Drawing.Size(138, 23)
        Me.txt_physicalOrder.TabIndex = 16
        Me.txt_physicalOrder.Text = "PHYSICAL + ON ORDER"
        '
        'txt_oldAdmin
        '
        Me.txt_oldAdmin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_oldAdmin.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_oldAdmin.ForeColor = System.Drawing.Color.Gray
        Me.txt_oldAdmin.Location = New System.Drawing.Point(1436, 15)
        Me.txt_oldAdmin.Name = "txt_oldAdmin"
        Me.txt_oldAdmin.PlaceHolderText = "Old Admin"
        Me.txt_oldAdmin.Size = New System.Drawing.Size(100, 23)
        Me.txt_oldAdmin.TabIndex = 15
        Me.txt_oldAdmin.Text = "OLD ADMIN"
        '
        'txt_totalCost
        '
        Me.txt_totalCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_totalCost.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_totalCost.ForeColor = System.Drawing.Color.Gray
        Me.txt_totalCost.Location = New System.Drawing.Point(1327, 15)
        Me.txt_totalCost.Name = "txt_totalCost"
        Me.txt_totalCost.PlaceHolderText = "Total Cost"
        Me.txt_totalCost.Size = New System.Drawing.Size(100, 23)
        Me.txt_totalCost.TabIndex = 14
        Me.txt_totalCost.Text = "TOTAL COST"
        '
        'txt_newAdmin
        '
        Me.txt_newAdmin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_newAdmin.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_newAdmin.ForeColor = System.Drawing.Color.Gray
        Me.txt_newAdmin.Location = New System.Drawing.Point(1544, 15)
        Me.txt_newAdmin.Name = "txt_newAdmin"
        Me.txt_newAdmin.PlaceHolderText = "New Admin"
        Me.txt_newAdmin.Size = New System.Drawing.Size(100, 23)
        Me.txt_newAdmin.TabIndex = 13
        Me.txt_newAdmin.Text = "NEW ADMIN"
        '
        'txt_volPrice
        '
        Me.txt_volPrice.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_volPrice.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_volPrice.ForeColor = System.Drawing.Color.Gray
        Me.txt_volPrice.Location = New System.Drawing.Point(1109, 15)
        Me.txt_volPrice.Name = "txt_volPrice"
        Me.txt_volPrice.PlaceHolderText = "Volume Price"
        Me.txt_volPrice.Size = New System.Drawing.Size(100, 23)
        Me.txt_volPrice.TabIndex = 12
        Me.txt_volPrice.Text = "VOLUME PRICE"
        '
        'txt_wholesale
        '
        Me.txt_wholesale.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_wholesale.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_wholesale.ForeColor = System.Drawing.Color.Gray
        Me.txt_wholesale.Location = New System.Drawing.Point(1000, 15)
        Me.txt_wholesale.Name = "txt_wholesale"
        Me.txt_wholesale.PlaceHolderText = "Wholesale"
        Me.txt_wholesale.Size = New System.Drawing.Size(100, 23)
        Me.txt_wholesale.TabIndex = 11
        Me.txt_wholesale.Text = "WHOLESALE"
        '
        'txt_com
        '
        Me.txt_com.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_com.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_com.ForeColor = System.Drawing.Color.Gray
        Me.txt_com.Location = New System.Drawing.Point(1218, 15)
        Me.txt_com.Name = "txt_com"
        Me.txt_com.PlaceHolderText = "COM"
        Me.txt_com.Size = New System.Drawing.Size(100, 23)
        Me.txt_com.TabIndex = 10
        Me.txt_com.Text = "COM"
        '
        'txt_rsp
        '
        Me.txt_rsp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_rsp.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_rsp.ForeColor = System.Drawing.Color.Gray
        Me.txt_rsp.Location = New System.Drawing.Point(784, 15)
        Me.txt_rsp.Name = "txt_rsp"
        Me.txt_rsp.PlaceHolderText = "RSP & Source"
        Me.txt_rsp.Size = New System.Drawing.Size(86, 23)
        Me.txt_rsp.TabIndex = 9
        Me.txt_rsp.Text = "RSP & SOURCE"
        '
        'txt_fullDesc
        '
        Me.txt_fullDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_fullDesc.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_fullDesc.ForeColor = System.Drawing.Color.Gray
        Me.txt_fullDesc.Location = New System.Drawing.Point(342, 15)
        Me.txt_fullDesc.Name = "txt_fullDesc"
        Me.txt_fullDesc.PlaceHolderText = "Full Description"
        Me.txt_fullDesc.Size = New System.Drawing.Size(432, 23)
        Me.txt_fullDesc.TabIndex = 8
        Me.txt_fullDesc.Text = "FULL DESCRIPTION"
        '
        'txt_costCurrency
        '
        Me.txt_costCurrency.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_costCurrency.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_costCurrency.ForeColor = System.Drawing.Color.Gray
        Me.txt_costCurrency.Location = New System.Drawing.Point(878, 15)
        Me.txt_costCurrency.Name = "txt_costCurrency"
        Me.txt_costCurrency.PlaceHolderText = "Cost in Currency"
        Me.txt_costCurrency.Size = New System.Drawing.Size(115, 23)
        Me.txt_costCurrency.TabIndex = 7
        Me.txt_costCurrency.Text = "COST IN CURRENCY"
        '
        'PlaceHolderTextBox1
        '
        Me.PlaceHolderTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PlaceHolderTextBox1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.PlaceHolderTextBox1.ForeColor = System.Drawing.Color.Gray
        Me.PlaceHolderTextBox1.Location = New System.Drawing.Point(150, 15)
        Me.PlaceHolderTextBox1.Name = "PlaceHolderTextBox1"
        Me.PlaceHolderTextBox1.PlaceHolderText = "Analysis Code"
        Me.PlaceHolderTextBox1.Size = New System.Drawing.Size(100, 23)
        Me.PlaceHolderTextBox1.TabIndex = 6
        Me.PlaceHolderTextBox1.Text = "ANALYSIS CODE"
        '
        'txt_product
        '
        Me.txt_product.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_product.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_product.ForeColor = System.Drawing.Color.Gray
        Me.txt_product.Location = New System.Drawing.Point(43, 15)
        Me.txt_product.Name = "txt_product"
        Me.txt_product.PlaceHolderText = "Product Code"
        Me.txt_product.Size = New System.Drawing.Size(100, 23)
        Me.txt_product.TabIndex = 5
        Me.txt_product.Text = "PRODUCT CODE"
        '
        'txt_quantity
        '
        Me.txt_quantity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_quantity.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.txt_quantity.ForeColor = System.Drawing.Color.Gray
        Me.txt_quantity.Location = New System.Drawing.Point(258, 15)
        Me.txt_quantity.Name = "txt_quantity"
        Me.txt_quantity.PlaceHolderText = "Quantity"
        Me.txt_quantity.Size = New System.Drawing.Size(75, 23)
        Me.txt_quantity.TabIndex = 3
        Me.txt_quantity.Text = "QUANTITY"
        '
        'POLines
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.RadSplitContainer1)
        Me.Name = "POLines"
        Me.Size = New System.Drawing.Size(1814, 89)
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadSplitContainer1.ResumeLayout(False)
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitPanel1.ResumeLayout(False)
        Me.SplitPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadSplitContainer1 As Telerik.WinControls.UI.RadSplitContainer
    Friend WithEvents SplitPanel1 As Telerik.WinControls.UI.SplitPanel
    Friend WithEvents txt_oldAdmin As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_totalCost As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_newAdmin As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_volPrice As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_wholesale As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_com As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_rsp As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_fullDesc As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_costCurrency As PO_Entry.PlaceHolderTextBox
    Friend WithEvents PlaceHolderTextBox1 As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_product As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_quantity As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_physicalOrder As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_outerBarcode As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_supplierCode As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_languages As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_code As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_gslCode As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_barcode As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_msds As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_exp As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_repeatable As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_webRestrict As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_mults As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_outer As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_inner As PO_Entry.PlaceHolderTextBox
    Friend WithEvents txt_pallet As PO_Entry.PlaceHolderTextBox

End Class
